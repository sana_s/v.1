<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon -->
<link rel="icon" href=".png">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Custom CSS File -->
<link rel="stylesheet" type="text/css" href="FCss.css">

<!-- Navbar -->
<br>
<br>
<br>
<br>
<footer class="navbar navbar-default navbar-fixed-bottom">

		<!-- Navbar Links Here -->
		<div class="collapse navbar-collapse">
		
			<!-- links here -->
			<ul class="nav navbar-nav navbar-right">
			
				<li><a href="about.php">About</a></li>
				<li><a href="contact.php">Contact</a></li>
				<li><a href="usage.php">Usage Policy</a></li>
				<li><a href="report.php">Report a Bug</a></li>
				
			</ul>

			
		</div>
	
</footer>
<!-- Navbar ends here -->