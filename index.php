<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Orienteering WebApp</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Custom CSS File -->
<link rel="stylesheet" type="text/css" href="PSite.css">

<script type="text/javascript">
	
function validate() {
	var username = document.loginForm.username.value;
	var password = document.loginForm.password.value;
				
	if(username == " " || username == "" || password == "" || password == " "){
		alert('Please enter a valid username and/or password');
				return false;
	}
	else{
		return true;
	}
}
</script>
</head>

<body>


<?php
  //include the header
   include('header.php');
   include('connect_to_db.php');
?>

<?php
 session_start(); // load the session variables if any

 
if (isset($_SESSION['login']) && $_SESSION['login']==1) {
//check if login successful
?> 
<br> <br> <br> <br> <br>
<center>
<form action="authenticate.php" method="POST"> 
<input type="submit" value="Logout" id="submitBtn" onClick="<?php session_destroy();?>"/> </a> 
</form> </center>
<?php
}
else {?>
<br> <br> <br> <br> <br>
<center>
<form action="authenticate.php" method="POST"> <a> Username: 
<input type="text" name="username" class="inputText" />
Password: <input type="password" name="password" class="inputText" />
<input type="submit" value="Login" id="submitBtn" onClick="return validate();"/> </a> 
</form> </center>
<?php } // end if
?>


<?php
  //include the footer
   include('footer.php');
?>

</body>

</html>