<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Favicon -->
<link rel="icon" href="Images/logo.png">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Custom CSS File -->
<link rel="stylesheet" type="text/css" href="PSite.css">

<!-- Navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">

	<!-- Opening Div container fluid for Navbar -->
	<div class="container-fluid">
	
		<!-- Navbar Header -->
		<div class="navbar-header">
			
			<!-- 3 Lines bar for mobile users -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			
			<!-- Header Link -->
			<a class="navbar-brand" href="index.php">
			</a>
			
		</div>
		
		<!-- Navbar Links Here -->
		<div class="collapse navbar-collapse" id="navbar">
		
			<!-- links here -->
			<ul class="nav navbar-nav">
				<li><a href="profile.php">Profile</a></li>
				<li><a href="register.php">Register</a></li>

				<li>
			<form class="navbar-form" role="search">
				
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search">
				</div>
					
				<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
				</form>
				</li>
			</ul>

		</div>
	
	</div>
	
</nav>

<br>
<br>
<br>
<br>
<!-- Navbar ends here -->
