<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Register </title>

<!-- Custom CSS File -->
<link rel="stylesheet" type="text/css" href="PSite.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</head>

<body>
<?php 
	include("header.php");
?>
<br> <br> <br> <br>
<form action = "result.php" method = "post">

<!-- Divs for User Details -->
        <div class="col-md-6 col-md-offset-3" style="border-style: groove; border-color: #DAA520; border-width: 5px;">

            <h3>1. Personal Information</h3>

            <div class="input-group">
                <input type="text" class="form-control" name="name" placeholder="Full Name" />
                <span class="input-group-addon">*</span>
            </div>

            <div class="input-group">
                <input type="text" class="form-control" name="address" placeholder="Address" />
                <span class="input-group-addon">*</span>
            </div>
			
			<div class="input-group">
                <input type="text" class="form-control" name="phone" placeholder="Phone Number" />
                <span class="input-group-addon">*</span>
            </div>
			
			<div class="input-group">
                <input type="text" class="form-control" name="emergency" placeholder="Emergency Contact Number" />
                <span class="input-group-addon">*</span>
            </div>

            <hr />

            <h3>2. Online Information</h3>

            <div class="input-group">
                <input type="text" class="form-control" name="email" placeholder="Email" />
                <span class="input-group-addon">*</span>
            </div>
			
            <div class="input-group">               
                <input type="text" class="form-control" name="username" placeholder="Username" />
                <span class="input-group-addon">*</span>
            </div>
			
			<div class="input-group">
                <input type="text" class="form-control" name="password" placeholder="Password" />
                <span class="input-group-addon">*</span>
            </div>

            <hr />
			
			 <input type = "submit" value = "Register" class = "col-md-6 col-md-offset-3 btn-primary">
			 
			<br> <br>
			
        </div>
		
</form>
<?php 
	include("footer.php");
?>		
</body>

</html>